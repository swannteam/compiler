package com.swannteam.compiler.parser;

import com.swannteam.compiler.symbols.Env;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Geliogabalus on 27.05.2015.
 */
public class CodeGenerator {

    public int ip = 0;
    public Hashtable<Integer, Integer> labels;
    public Hashtable<Integer, ArrayList<Integer>> undefLabels;
    public ArrayList<Var> vars;
    public ArrayList<Var> tempvars;
    public ArrayList<Integer> opcodes;
    public Hashtable<String, Address> proctable;
    public Hashtable<Integer, String> stringtable;
    public Address Handle = null;
    public Address Written = null;

    public void addOp(int op){
        opcodes.add(op);
        ip++;
    }


    public void print(String s){
            Handle = new Address(Parser.used);
            Parser.used += 4;
            Written = new Address(Parser.used);
            Parser.used += 4;
            addOp(0x68);
            addOp(0xF5);
            addOp(0xFF);
            addOp(0xFF);
            addOp(0xFF);
            addOp(0xFF);
            addOp(0x15);
            addAddr(proctable.get("GetStdHandle"), 2);
            addOp(0xA3);
            addAddr(Handle, 1);
            stringtable.put(Parser.used, s);
            addOp(0x68);
            addImm(0);
            addOp(0x68);
            addAddr(Written, 1);
            addOp(0x68);
            addImm(s.length());
            addOp(0x68);
            addAddr(new Address(Parser.used), 1);
            Parser.used += s.length() + 1;
            addOp(0xA1);
            addAddr(Handle, 1);
            addOp(0x50);
            addOp(0xFF);
            addOp(0x15);
            addAddr(proctable.get("WriteConsoleA"), 2);
    }

    public void printNum(){

    }

    public void addAddr(Address addr, int type){
        int tmpa = addr.GetAdds();
        switch (type) {
            case 1:
                tmpa += 0x00003000;
                break;
            case 2:
                tmpa += 0x00000000;
                break;
        }
        int tmp;
        for(int i = 0; i < 4; ++i) {
            tmp = tmpa % 0x100;
            opcodes.add(tmp);
            tmpa = tmpa / 0x100;
            ++ip;
        }
    }

    public void addImm(int num){
        int tmp;
        for(int i = 0; i < 4; i++){
            tmp = num % 0xff;
            opcodes.add(tmp);
            num = num / 0xff;
            ip++;
        }
    }

    public void addOffset(int num){
        boolean sign;
        sign = num > 0;
        int tmp;
        for(int i = 0; i < 4; i++){
            tmp = num % 0xff;
            if(tmp == 0){
                if (!sign)
                    tmp = 0xff;
            }
            opcodes.add(tmp);
            num = num / 0xff;
            ip++;
        }
    }

    public void addOffsetAddr(int off, int num){
        int tmp;
        for(int i = 0; i < 4; i++){
            tmp = num % 0xff;
            opcodes.set(off + i, tmp);
            num = num / 0xff;
        }
    }

    public Var findVar(String name, Env env){
        for (Var var1 : vars) {
            if ((var1.name.equals(name)) & (var1.env == env)) {
                return var1;
            }
        }
        for (Var var : vars) {
            if (var.name.equals(name)) {
                return var;
            }
        }
        return null;
    }

    public Var findTempVar(String name){
        for (Var var : tempvars) {
            if (var.name.equals(name)) {
                return var;
            }
        }
        return null;
    }

    private void accessNode(String scode, Env env) {
        String[] buf = scode.split("=");
        buf[0] = buf[0].trim();
        buf[1] = buf[1].trim();
        int leftBracket = buf[1].indexOf('[');
        int rightBracket = buf[1].indexOf(']');
        String masName = buf[1].substring(0, leftBracket);
        String offsetName = buf[1].substring(leftBracket + 1, rightBracket);
        Var masVar = findVar(masName, env);
        Var offsetVar = findTempVar(offsetName);

        Var left = findVar(buf[0], env);
        if (left == null) {
            left = findTempVar(buf[0]);
        }

        addOp(0x8D);
        addOp(0x35);
        addAddr(masVar.address, 1);
        addOp(0xA1);
        addAddr(offsetVar.address, 1);
        addOp(0x8B);
        addOp(0x04);
        addOp(0x30);
        addOp(0xA3);
        addAddr(left.address, 1);
    }

    private void lazyLabel(int label) {
        if (labels.get(label) == null) {
            if (undefLabels.get(label) == null) {
                undefLabels.put(label, new ArrayList());
            }
            undefLabels.get(label).add(ip);
            addImm(0);
        } else {
            addOffset(labels.get(label) - ip - 4);
        }
    }

    public void sendlabel(int num){
        labels.put(num, ip);

        ArrayList undef = undefLabels.get(num);
        if (undef != null) {
            for (int i = 0; i < undef.size(); ++i) {
                addOffsetAddr((Integer)undef.get(i),ip - (Integer)undef.get(i) - 4);
            }
            undefLabels.remove(num);
        }
    }

    public void send(String scode, Class node, Env env){
        String[] buf;
        String[] oper;
        Var right1 = null;
        Var right2 = null;
        Var left;
        Integer imm1 = null;
        Integer imm2 = null;
        switch (node.getSimpleName()) {
            case "SetElem":
                int leftBracket = scode.indexOf('[');
                int rightBracket = scode.indexOf(']');
                String masName = scode.substring(0, leftBracket);
                String offsetName = scode.substring(leftBracket + 1, rightBracket);
                Var masVar = findVar(masName, env);
                Var offsetVar = findTempVar(offsetName);

                buf = scode.split("=");
                buf[1] = buf[1].trim();
                if (Character.isLetter(buf[1].charAt(0))) {
                    right1 = findVar(buf[1], env);
                    if (right1 == null){
                        right1 = findTempVar(buf[1]);
                    }
                } else {
                    imm1 = Integer.parseInt(buf[1]);
                }

                addOp(0x8D);
                addOp(0x35);
                addAddr(masVar.address, 1);
                addOp(0x8B);
                addOp(0x1D);
                addAddr(offsetVar.address, 1);
                if (right1 != null) {
                    addOp(0xA1);
                    addAddr(right1.address, 1);
                } else {
                    addOp(0xB8);
                    addImm(imm1);
                }
                addOp(0x89);
                addOp(0x04);
                addOp(0x33);
                return;
            case "Access":
                accessNode(scode, env);
                return;
            case "Set":
                if (scode.indexOf('[') != -1) {
                    accessNode(scode, env);
                    return;
                }
            case "Arith":
                buf = scode.split("=");
                buf[0] = buf[0].trim();
                left = findVar(buf[0], env);
                if (left == null){
                    left = findTempVar(buf[0]);
                }
                buf[1] = buf[1].trim();
                if ((oper = buf[1].split(" ")).length > 1){
                    if(Character.isDigit(oper[0].charAt(0)) && Character.isDigit(oper[2].charAt(0))){
                        imm1 = Integer.parseInt(oper[0]);
                        imm2 = Integer.parseInt(oper[2]);
                    }
                    if(Character.isDigit(oper[0].charAt(0)) && Character.isLetter(oper[2].charAt(0))){
                        right2 = findVar(oper[2], env);
                        if (right2 == null){
                            right2 = findTempVar(oper[2]);
                        }
                        imm1 = Integer.parseInt(oper[0]);
                    }
                    if(Character.isLetter(oper[0].charAt(0)) && Character.isDigit(oper[2].charAt(0))){
                        right1 = findVar(oper[0], env);
                        if (right1 == null){
                            right1 = findTempVar(oper[0]);
                        }
                        imm2 = Integer.parseInt(oper[2]);
                    }
                    if(Character.isLetter(oper[0].charAt(0)) && Character.isLetter(oper[2].charAt(0))){
                        right1 = findVar(oper[0], env);
                        if (right1 == null){
                            right1 = findTempVar(oper[0]);
                        }
                        right2 = findVar(oper[2], env);
                        if (right2 == null){
                            right2 = findTempVar(oper[2]);
                        }
                    }
                    switch (oper[1]){
                        case "+":
                            if ((right1 == null) & (right2 == null)){
                                addOp(0xC7);
                                addOp(0x05);
                                addAddr(left.address, 1);
                                addImm(imm1 + imm2);
                                return;
                            }
                            if (right2 == null){
                                addOp(0xA1);
                                addAddr(right1.address, 1);
                                addOp(0x05);
                                addImm(imm2);
                                addOp(0xA3);
                                addAddr(left.address, 1);
                                return;
                            }
                            if (right1 == null){
                                addOp(0xA1);
                                addAddr(right2.address, 1);
                                addOp(0x05);
                                addImm(imm1);
                                addOp(0xA3);
                                addAddr(left.address, 1);
                                return;
                            }
                            addOp(0xA1);
                            addAddr(right1.address, 1);
                            addOp(0x03);
                            addOp(0x05);
                            addAddr(right2.address, 1);
                            addOp(0xA3);
                            addAddr(left.address, 1);
                            return;
                        case"-":
                            if((right1 == null) & (right2 == null)){
                                addOp(0xC7);
                                addOp(0x05);
                                addAddr(left.address, 1);
                                addImm(imm1 - imm2);
                                return;
                            }
                            if(right2 == null){
                                addOp(0xA1);
                                addAddr(right1.address, 1);
                                addOp(0x2D);
                                addImm(imm2);
                                addOp(0xA3);
                                addAddr(left.address, 1);
                                return;
                            }
                            if(right1 == null){
                                addOp(0xA1);
                                addAddr(right2.address, 1);
                                addOp(0xF7);
                                addOp(0xD8);
                                addOp(0x05);
                                addImm(imm1);
                                addOp(0xA3);
                                addAddr(left.address, 1);
                                return;
                            }
                            addOp(0xA1);
                            addAddr(right1.address, 1);
                            addOp(0x2B);
                            addOp(0x05);
                            addAddr(right2.address, 1);
                            addOp(0xA3);
                            addAddr(left.address, 1);
                            return;
                        case"*":
                            if ((right1 == null) && (right2 == null)) {
                                addOp(0xC7);
                                addOp(0x05);
                                addAddr(left.address, 1);
                                addImm(imm1 * imm2);
                                return;
                            }
                            if (right2 == null) {
                                addOp(0xA1);
                                addAddr(right1.address, 1);
                                addOp(0x69);
                                addOp(0xC0);
                                addImm(imm2);
                                addOp(0xA3);
                                addAddr(left.address, 1);
                                return;
                            }
                            if (right1 == null) {
                                addOp(0xA1);
                                addAddr(right2.address, 1);
                                addOp(0x69);
                                addOp(0xC0);
                                addImm(imm1);
                                addOp(0xA3);
                                addAddr(left.address, 1);
                                return;
                            }
                            addOp(0xA1);
                            addAddr(right1.address, 1);
                            addOp(0x0F);
                            addOp(0xAF);
                            addOp(0x05);
                            addAddr(right2.address, 1);
                            addOp(0xA3);
                            addAddr(left.address, 1);
                            return;
                        case"/":
                            if ((right1 == null) && (right2 == null)) {
                                addOp(0xC7);
                                addOp(0x05);
                                addAddr(left.address, 1);
                                addImm(imm1 / imm2);
                                return;
                            }
                            if (right2 == null) {
                                addOp(0xA1);
                                addAddr(right1.address, 1);
                                addOp(0xBA);
                                addImm(0);
                                addOp(0xBB);
                                addImm(imm2);
                                addOp(0xF7);
                                addOp(0xFB);
                                addOp(0xA3);
                                addAddr(left.address, 1);
                                return;
                            }
                            if (right1 == null) {
                                addOp(0xB8);
                                addImm(imm1);
                                addOp(0xBA);
                                addImm(0);
                                addOp(0x8B);
                                addOp(0x1D);
                                addAddr(right2.address, 1);
                                addOp(0xF7);
                                addOp(0xFB);
                                addOp(0xA3);
                                addAddr(left.address, 1);
                                return;
                            }
                            addOp(0xA1);
                            addAddr(right1.address, 1);
                            addOp(0xBA);
                            addImm(0);
                            addOp(0xBB);
                            addAddr(right2.address, 1);
                            addOp(0xF7);
                            addOp(0xFB);
                            addOp(0xA3);
                            addAddr(left.address, 1);
                            return;
                    }
                }
                else {
                    oper[0] = oper[0].trim();
                    if(Character.isDigit(oper[0].charAt(0))){
                        addOp(0xC7);
                        addOp(0x05);
                        addAddr(left.address, 1);
                        addImm(Integer.parseInt(oper[0]));
                        return;
                    }
                    else {
                        Var right = findVar(oper[0], env); //a1 00 20 40 00 8b 1d 00 20 40 00 8b 0d 00 20 40 00 8b 15 00 20 40 00 eax, ebx, ecx, edx
                        addOp(0xA1);
                        addAddr(right.address, 1);
                        addOp(0xA3);
                        addAddr(left.address, 1);
                        return;
                    }
                }
                break;
            case "Break":
            case "While":
            case "Else":
                String label = scode.trim();
                label = label.substring(6, label.length());
                Integer labelNum = Integer.parseInt(label);
                addOp(0xE9);
                lazyLabel(labelNum);
                break;
            case "Rel":
                String[] mas = scode.split(" ");
                Boolean rel = true;
                Boolean strict = true;
                labelNum = Integer.parseInt(mas[5].substring(1));

                if (mas[2].length() == 2) {
                    strict = false;
                }
                if (mas[2].charAt(0) == '<') {
                    rel = !rel;
                }
                if (mas[0].equals("iffalse")) {
                    rel = !rel;
                    strict = !strict;
                }


                if (Character.isLetter(mas[1].charAt(0))) {
                    right1 = findVar(mas[1], env);
                    if (right1 == null){
                        right1 = findTempVar(mas[1]);
                    }
                } else {
                    imm1 = Integer.parseInt(mas[1]);
                }
                if (Character.isLetter(mas[3].charAt(0))) {
                    right2 = findVar(mas[3], env);
                    if (right2 == null){
                        right2 = findTempVar(mas[3]);
                    }
                } else {
                    imm2 = Integer.parseInt(mas[3]);
                }

                if (right1 != null) {
                    addOp(0xA1);
                    addAddr(right1.address, 1);
                } else {
                    addOp(0xB8);
                    addImm(imm1);
                }
                if (right2 != null) {
                    addOp(0x8B);
                    addOp(0x1D);
                    addAddr(right2.address, 1);
                } else {
                    addOp(0xBB);
                    addImm(imm2);
                }

                addOp(0x3B);
                addOp(0xC3);

                addOp(0x0F);
                if (mas[2].charAt(0) == '=') {
                    if (rel) {
                        addOp(0x84);
                    } else {
                        addOp(0x85);
                    }
                } else {
                    if (strict) {
                        //>
                        if (rel) {
                            addOp(0x8F);
                        }
                        //<
                        else {
                            addOp(0x8C);
                        }
                    } else {
                        //>=
                        if (rel) {
                            addOp(0x8D);
                        }
                        //<=
                        else {
                            addOp(0x8E);
                        }
                    }
                }
                lazyLabel(labelNum);

                break;
            case "Print":
                if (scode.indexOf('[') != -1) {
                    //printMasElem(scode, env);
                    //return;
                }
                print(scode.substring(3,scode.length()-1));
        }
    }


    public void fin(){

    }

    public CodeGenerator() {
        vars = new ArrayList<>();
        tempvars = new ArrayList<>();
        labels = new Hashtable<>();
        opcodes = new ArrayList<>();
        undefLabels = new Hashtable<>();
        proctable = new Hashtable<>();
        proctable.put("ExitProcess", new Address(0x2038));
        proctable.put("GetStdHandle", new Address(0x203c));
        proctable.put("WriteConsoleA", new Address(0x2040));
        stringtable = new Hashtable<>();
    }
}