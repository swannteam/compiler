package com.swannteam.compiler.parser;

import com.swannteam.compiler.parser.Address;
import com.swannteam.compiler.symbols.Env;

/**
 * Created by Geliogabalus on 27.05.2015.
 */
public class Var{
    public Address address;
    public Env env;
    public String name;
    public Var(Address address, Env env, String name) {
        this.address = address;
        this.env = env;
        this.name = name;
    }
}
