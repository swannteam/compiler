package com.swannteam.compiler.parser;

/**
 * Created by Geliogabalus on 27.05.2015.
 */
public class Address {
    public static int baseAdds = 0x00400000;
    private int adds;
    public Address(int adds){
        this.adds = baseAdds + adds;
    }
    public int GetAdds(){
        return adds;
    }
    public void Add(int adding){
        adds += adding;
    }
}
