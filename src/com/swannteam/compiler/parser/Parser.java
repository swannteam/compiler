package com.swannteam.compiler.parser;

import com.swannteam.compiler.inter.*;
import com.swannteam.compiler.lexer.*;
import com.swannteam.compiler.symbols.Env;
import com.swannteam.compiler.symbols.Type;

import java.util.ArrayList;

public class Parser {
    private Lexer lex;
    public static CodeGenerator codegen;
    private Token look;
    private int currentTok = 0;
    private ArrayList<Token> tokens;
    public static Env top;
    public static Env blockEnv;
    public static int used;

    public Parser(ArrayList<Token> tokens) {
        this.tokens = tokens;
        codegen = new CodeGenerator();
        used = 0;
        top = null;
        move();
    }

    void move() {
        if(tokens.size() > currentTok) {
            look = tokens.get(currentTok++);
        }
    }

    void error(String s) {
        throw new Error("near line" + lex.line + ": " + s);
    }

    void match(int t) {
        if (look.getTag() == t)
            move();
        else
            error("syntax error");
    }

    public void program() {
        Stmt s = block();
        int begin = s.newlabel();
        int after = s.newlabel();
        s.emitLabel(begin);
        s.gen(begin, after);
        s.emitLabel(after);
    }

    Stmt block() {
        match('{');
        Env savedEnv = top;
        top = new Env(top);
        blockEnv = top;
        decls();
        Stmt s = stmts();
        match('}');
        top = savedEnv;
        return s;
    }

    void decls() {
        while (look.getTag() == Tag.BASIC) {
            Type p = type();
            Token tok = look;
            match(Tag.ID);
            match(';');
            Id id = new Id((Word)tok, p, used);
            codegen.vars.add(new Var(new Address(used), top, ((Word)tok).lexeme));
            top.put(tok, id);
            used = used + p.width;
        }
    }

    Type type() {
        Type p = (Type)look;
        match(Tag.BASIC);
        if (look.getTag() != '[')
            return p;
        else
            return dims(p);
    }

    Type dims(Type p) {
        match('[');
        Token tok = look;
        match((Tag.NUM));
        match(']');
        if (look.getTag() == '[')
            p = dims(p);
        return new Array(((Num)tok).value, p);
    }

    Stmt stmts() {
        if (look.getTag() == '}')
            return Stmt.Null;
        else
            return new Seq(stmt(), stmts());
    }

    Stmt stmt() {
        Expr x;
        Stmt s, s1, s2;
        Stmt savedStmt;

        switch (look.getTag()) {
            case ';':
                move();
                return Stmt.Null;
            case Tag.PRINT:
                match(Tag.PRINT);
                match('(');
                x = bool();
                match(')');
                match(';');
                return new Print(x);
            case Tag.IF:
                match(Tag.IF);
                match('(');
                x = bool();
                match(')');
                s1 = stmt();
                if (look.getTag() != Tag.ELSE)
                    return new If(x, s1);
                match(Tag.ELSE);
                s2 = stmt();
                return new Else(x, s1, s2);
            case Tag.WHIlE:
                While whilenode = new While();
                savedStmt = Stmt.Enclosing;
                Stmt.Enclosing = whilenode;
                match(Tag.WHIlE);
                match('(');
                x = bool();
                match(')');
                s1 = stmt();
                whilenode.init(x, s1);
                Stmt.Enclosing = savedStmt;
                return whilenode;
            case Tag.DO:
                Do donode = new Do();
                savedStmt = Stmt.Enclosing;
                Stmt.Enclosing = donode;
                match(Tag.DO);
                s1 = stmt();
                match(Tag.WHIlE);
                match('(');
                x = bool();
                match(')');
                match(';');
                donode.init(s1, x);
                Stmt.Enclosing = savedStmt;
                return donode;
            case Tag.BREAK:
                match(Tag.BREAK);
                match(';');
                return new Break();
            case '{':
                return block();
            default:
                return assign();
        }
    }

    Stmt assign() {
        Stmt stmt;
        Token t = look;
        match(Tag.ID);
        Id id = top.get(t);
        if (id == null)
            error(t.toString() + " undeclared");
        if (look.getTag() == '=') {
            move();
            stmt = new Set(id, bool());
        }
        else {
            Access x = offset(id);
            match('=');
            stmt = new SetElem(x, bool());
        }
        match(';');
        return stmt;
    }

    Expr bool() {
        Expr x = join();
        while (look.getTag() == Tag.OR) {
            Token tok = look;
            move();
            x = new Or(tok, x, join());
        }
        return x;
    }

    Expr join() {
        Expr x = equality();
        while (look.getTag() == Tag.AND) {
            Token tok = look;
            move();
            x = new And(tok, x, equality());
        }
        return x;
    }

    Expr equality() {
        Expr x = rel();
        while (look.getTag() == Tag.EQ || look.getTag() == Tag.NE) {
            Token tok = look;
            move();
            x = new Rel(tok, x, rel());
        }
        return x;
    }

    Expr rel() {
        Expr x = expr();
        switch (look.getTag()) {
            case '<':
            case Tag.LE:
            case Tag.GE:
            case '>':
                Token tok = look;
                move();
                return new Rel(tok, x, expr());
            default:
                return x;
        }
    }

    Expr expr() {
        Expr x = term();
        while (look.getTag() == '+' || look.getTag() == '-') {
            Token tok = look;
            move();
            x = new Arith(tok, x, term());
        }
        return x;
    }

    Expr term() {
        Expr x = unary();
        while (look.getTag() == '*' || look.getTag() == '/') {
            Token tok = look;
            move();
            x = new Arith(tok, x, unary());
        }
        return x;
    }

    Expr unary() {
        if (look.getTag() == '-') {
            move();
            return new Unary(Word.minus, unary());
        }
        else if (look.getTag() == '!') {
            Token tok = look;
            move();
            return new Not(tok, unary());
        }
        else
            return factor();
    }

    Expr factor() {
        Expr x;
        switch (look.getTag()) {
            case '(':
                move();
                x = bool();
                match(')');
                return x;
            case Tag.NUM:
                x = new Constant(look, Type.Int);
                move();
                return x;
            case Tag.REAL:
                x = new Constant(look, Type.Float);
                move();
                return x;
            case Tag.STRING:
                x = new Constant(look, Type.String);
                move();
                return x;
            case Tag.TRUE:
                x = Constant.True;
                move();
                return x;
            case Tag.FALSE:
                x = Constant.False;
                move();
                return x;
            default:
                error("syntax error");
            case Tag.ID:
                String s = look.toString();
                Id id = top.get(look);
                if (id == null)
                    error(look.toString() + " undeclared");
                move();
                if (look.getTag() != '[')
                    return id;
                else
                    return offset(id);
        }
    }

    Access offset(Id a) {
        Expr i;
        Expr w;
        Expr t1, t2;
        Expr loc;
        Type type = a.type;
        match('[');
        i = bool();
        match(']');
        type = ((Array)type).of;
        w = new Constant(type.width);
        t1 = new Arith(new Token('*'), i, w);
        loc = t1;
        while (look.getTag() == '[') {
            match('[');
            i = bool();
            match(']');
            type = ((Array)type).of;
            w = new Constant(type.width);
            t1 = new Arith(new Token('*'), i, w);
            t2 = new Arith(new Token('+'), loc, t1);
            loc = t2;
        }
        return new Access(a, loc, type);
    }
}