package com.swannteam.compiler.peconstructor;

import com.swannteam.compiler.parser.Parser;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Geliogabalus on 21.05.2015.
 */
public class PEconstructor {
    private OutputStream file;
    private int datasize;
    private int codesize;
    private int virtcodesize;
    private int virtdatasize;
    private int imagesize;
    private ArrayList<Integer> code;
    private byte[] data;
    private ArrayList<Integer> rdata;
    private int headerlength;
    private Hashtable<Integer, String> stringtable;
    private static int[] startheader = {
            0x4d, 0x5a, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x40, 0, 0, 0,
            0x50, 0x45, 0, 0,
            0x4c, 0x01, 0x03, 0,
            0, 0, 0, 0, //time
            0, 0, 0, 0, 0, 0, 0, 0,
            0xe0, 0, //size of optional
            0b00001111,
            0b00000001,
            0x0b, 0x01,
            0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0x10, 0, 0,//entry point
            0, 0, 0, 0,
            0, 0, 0, 0,

            0, 0, 0x40, 0,
            0, 0x10, 0, 0, //sectalign
            0, 0x02, 0, 0, //filealign
            0x06, 0, 0, 0, //OS verion
            0, 0, 0, 0,
            0x06, 0, 0, 0,
            0, 0, 0, 0,
            0, 0x40, 0, 0, //Size of image
            0, 0x02, 0, 0,
            0, 0, 0, 0,
            0x03, 0, //subsystem
            0x40, 0x81,
            0, 0, 0x10, 0,
            0, 0x01, 0, 0,
            0, 0, 0x10, 0,
            0, 0x01, 0, 0,
            0, 0, 0, 0,
            0x10, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0x20, 0, 0, //import table
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, //reloc
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
    };

    private static int[] sectiontext = {
            0,0x10,0,0,
            0,0x10,0,0,
            0,0x02,0,0, //size of code
            0,0x04,0,0, //code pointer
            0,0,0,0,0,0,0,0,0,0,0,0,
            0x20,0,0,0x60
    };


    private static int[] sectiondata = {
            0,0x10,0,0,
            0,0x30,0,0,
            0,0x02,0,0, //size of data
            0,0x08,0,0, //data pointer
            0,0,0,0,0,0,0,0,0,0,0,0,
            0x40,0,0,0xc0
    };

    private static int[] sectionrdata = {
            0,0x10,0,0,
            0,0x20,0,0,
            0,0x02,0,0, //size of data
            0,0x06,0,0, //data pointer
            0,0,0,0,0,0,0,0,0,0,0,0,
            0x40,0,0,0x40
    };

    private static int[] fincode = {
            /*0x6a,0,0x68,00,0x30,0x40,0,0x68,0x17,0x30,0x40,0,0x6A,0x00,0xFF,0x15,
            0x70,0x20,0x40,0,*/0x6A,0,0xFF,0x25,0x38,0x20,0x40,0
    };



    public PEconstructor(String filename, ArrayList<Integer> code, int used, Hashtable<Integer, String> stringtable) {
        try {
            file = new FileOutputStream(filename);
        } catch (Exception e) {
            System.out.println("File error!");
        }
        this.code = code;
        rdata = new ArrayList<Integer>();
        this.stringtable = stringtable;
        codesize = (code.size() / 0x200) * 0x200 + 0x200;
        datasize = (used / 0x200) * 0x200 + 0x200;
        virtcodesize = (codesize / 0x1000) * 0x1000 + 0x1000;
        virtdatasize = (datasize / 0x1000) * 0x1000 + 0x1000;
        imagesize = 0x1000 + virtcodesize + virtdatasize + 0x1000;
        startheader[0x42 + 20 + 59] = imagesize / 0x100;
        sectiontext[1] = virtcodesize / 0x100;
        sectiontext[9] = codesize / 0x100;
        sectionrdata[5] = (virtcodesize + 0x1000) / 0x100;
        sectionrdata[13] = (codesize + 0x400) / 0x100;
        sectiondata[1] = virtdatasize / 0x100;
        sectiondata[9] = datasize / 0x100;
        sectiondata[13] = (codesize + 0x600) / 0x100;
    }

    public void construct() throws IOException{
        constructHeader();
        constructSectionTable();
        headerlength = startheader.length + sectiondata.length + sectiontext.length + sectionrdata.length + 24;
        for(int i = 0; i < 0x400 - headerlength;i++){
            file.write(0);
        }
        insertCode();
        for(int i = 0; i < codesize - (code.size() + fincode.length);i++){
            file.write(0);
        }
        insertRdata();
        for(int i = 0; i < 0x200 - rdata.size();i++){
            file.write(0);
        }
        insertData();
        for(int i = 0; i < datasize - (Parser.used ); i++){
            file.write(0);
        }
    }

    public void constructHeader() throws IOException {
        for (int i = 0; i < startheader.length; i++) {
            int tmp = startheader[i];
            file.write(tmp);
        }
    }

    public void insertCode() throws IOException{
        for (int i = 0; i < code.size(); i++) {
            int tmp = code.get(i);
            file.write(tmp);
        }
        for (int i = 0; i < fincode.length; i++) {
            int tmp = fincode[i];
            file.write(tmp);
        }
    }

    public void addOffs(Offset offs) throws IOException {
        int tmpa = offs.GetOffs();
        tmpa += virtcodesize + 0x1000;
        int tmp;
        for(int i = 0; i < 4; i++){
            tmp = tmpa % 0x100;
            rdata.add(tmp);
            tmpa = tmpa / 0x100;
        }
    }

    public void insertRdata() throws  IOException{
        Offset[] kernelimpaddr = {
                new Offset(0),
                new Offset(0),
                new Offset(0)
        };
        Offset[] addresses = {
                new Offset(0),
                new Offset(0),
                new Offset(0)
        };
        String[] dll = {
                "kernel32.dll"
        };
        String[] functions = {
                "ExitProcess",
                "GetStdHandle",
                "WriteConsoleA"
        };
        ArrayList<Integer> newrdata = new ArrayList<>();
        for(int i = 0; i < functions.length; i++){
            byte[] mas;
            newrdata.add(0);
            newrdata.add(0);
            mas = functions[i].getBytes();
            for(int j = 0; j < mas.length; j++){
                newrdata.add((int) mas[j]);
            }
            newrdata.add(0);
        }
        for(int i = 1; i < addresses.length; i++){
            addresses[i].Add(functions[i-1].length() + 3 + addresses[i - 1].GetOffs());
        }
        for(int i = 0; i < addresses.length; i++){
            addresses[i].Add(dll.length*20+20 + (addresses.length + 1) * 8);
        }

        kernelimpaddr[0].Add(dll.length*20 + 20);
        kernelimpaddr[1].Add(dll.length*20 + 20 + (addresses.length + 1) * 8 + newrdata.size());
        kernelimpaddr[2].Add(dll.length*20 + 20 + (addresses.length + 1) * 4);
        for(int i = 0; i < dll.length; i++){
            byte[] mas;
            mas = dll[i].getBytes();
            for(int j = 0; j < mas.length; j++){
                newrdata.add((int) mas[j]);
            }
            newrdata.add(0);
        }

        addOffs(kernelimpaddr[0]);
        for(int i = 0; i < 8; i++){
            rdata.add(0);
        }
        addOffs(kernelimpaddr[1]);
        addOffs(kernelimpaddr[2]);
        for(int i = 0; i < 20; i++){
            rdata.add(0);
        }
        for(int i = 0; i < addresses.length; i++){
            addOffs(addresses[i]);
        }
        for(int i = 0; i < 4; i++){
            rdata.add(0);
        }
        for(int i = 0; i < addresses.length; i++){
            addOffs(addresses[i]);
        }
        for(int i = 0; i < 4; i++){
            rdata.add(0);
        }
        for(int i = 0; i < newrdata.size(); i++){
            int tmp = newrdata.get(i);
            rdata.add(tmp);
        }
        for(int i = 0; i < rdata.size(); i++){
            int tmp = rdata.get(i);
            file.write(tmp);
        }
    }

    public void insertData() throws  IOException{
        int[] data = new int[Parser.used];
        Integer[] stringaddr = new Integer[stringtable.size()];
        stringtable.keySet().toArray(stringaddr);
        for (int i = 0; i < stringtable.keySet().size(); i++){
            for(int j = 0; j < stringtable.get(stringaddr[i]).length(); j++){
                data[stringaddr[i] + j] = stringtable.get(stringaddr[i]).charAt(j);
            }
            data[stringaddr[i] + stringtable.get(stringaddr[i]).length()] = 0;
        }
        //testdata = "a simple PE executable\0Hello World\0".getBytes();
        //data = testdata;
        for (int i = 0; i < data.length; i++) {
            int tmp = data[i];
            file.write(tmp);
        }
        //file.write(data);
    }

    public void constructSectionTable() throws  IOException{
        file.write(".text\0\0\0".getBytes());
        for (int i = 0; i < sectiontext.length; i++) {
            int tmp = sectiontext[i];
            file.write(tmp);
        }
        file.write(".rdata\0\0".getBytes());
        for (int i = 0; i < sectionrdata.length; i++) {
            int tmp = sectionrdata[i];
            file.write(tmp);
        }
        file.write(".data\0\0\0".getBytes());
        for (int i = 0; i < sectiondata.length; i++) {
            int tmp = sectiondata[i];
            file.write(tmp);
        }
    }

    public void close() {
        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
