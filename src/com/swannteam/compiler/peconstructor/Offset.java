package com.swannteam.compiler.peconstructor;

/**
 * Created by Geliogabalus on 09.09.2015.
 */
public class Offset {
    private int offs;
    public Offset(int offs){
        this.offs = offs;
    }
    public int GetOffs(){
        return offs;
    }
    public void Add(int adding){
        offs += adding;
    }
}
