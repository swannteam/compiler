package com.swannteam.compiler.lexer;

import com.swannteam.compiler.symbols.Type;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Geliogabalus on 03.04.2015.
 */
public class Lexer {
    public static int line = 1;
    private HashMap<Integer,Pattern> numPatterns;
    private HashMap<Integer,Pattern> patterns;
    private Pattern idpattern;

    public Lexer() {
        numPatterns = new HashMap<>();
        numPatterns.put(Tag.NUM, Pattern.compile("[0-9]+"));
        numPatterns.put(Tag.REAL, Pattern.compile("[0-9]*\\.[0-9]*"));

        patterns = new HashMap<>();
        patterns.put(Tag.FOR, Pattern.compile("\\W*for\\W"));
        patterns.put(Tag.IF, Pattern.compile("\\W*if\\W"));
        patterns.put(Tag.ELSE, Pattern.compile("\\W*else\\W"));
        patterns.put(Tag.DO, Pattern.compile("\\W*do\\W"));
        patterns.put(Tag.TRUE, Pattern.compile("\\W*true\\W"));
        patterns.put(Tag.FALSE, Pattern.compile("\\W*false\\W"));
        patterns.put(Tag.WHIlE, Pattern.compile("\\W*while\\W"));
        patterns.put(Tag.PRINT, Pattern.compile("\\W*print\\W"));
        patterns.put(Tag.BREAK, Pattern.compile("\\W*break\\W"));
        patterns.put(Tag.CONTINUE, Pattern.compile("\\W*continue\\W"));
        patterns.put(Tag.STRING, Pattern.compile("\".*\""));
        patterns.put(Tag.BASIC, Pattern.compile("\\W*int\\W|\\W*float\\W|\\W*char\\W|\\W*bool\\W"));

        idpattern = Pattern.compile("[_a-zA-Z]+[_a-zA-Z0-9]*");
    }

    public ArrayList<Token> scan(BufferedReader stream){
        ArrayList<Token> tokens = new ArrayList<>();
        String line;
        try {
            while((line = stream.readLine()) != null) {
                tokens.addAll(getTokens(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tokens;
    }

    public ArrayList<Token> getTokens(String current_line){
        ArrayList<Token> tempList = new ArrayList<>();
        char[] str = (current_line + '\n').toCharArray();
        for(int i = 0; i < current_line.length(); ++i){
            if (str[i] == '\n'){
                break;
            }
            next: switch(str[i]){
                case ' ':
                    break;
                case '\t':
                    break;
                case ';':
                    tempList.add(new Token(';'));
                    break;
                case '{':
                    tempList.add(new Token('{'));
                    break;
                case '}':
                    tempList.add(new Token('}'));
                    break;
                case ',':
                    tempList.add(new Token(','));
                    break;
                case '[':
                    tempList.add(new Token('['));
                    break;
                case ']':
                    tempList.add(new Token(']'));
                    break;
                case '(':
                    tempList.add(new Token('('));
                    break;
                case ')':
                    tempList.add(new Token(')'));
                    break;
                case '/':
                    if (str[i + 1] == '/') {
                        str[i + 1] = '\n';
                    }
                    else{
                        tempList.add(new Token('/'));
                    }
                    break;
                case '=':
                    if (str[i + 1] == '='){
                        tempList.add(Word.eq);
                        i++;
                    }
                    else{
                        tempList.add(new Token('='));
                    }
                    break;
                case '+':
                    if (str[i + 1] == '+'){
                        tempList.add(new Token(Tag.INC));
                        i++;
                    }
                    else{
                        tempList.add(new Token('+'));
                    }
                    break;
                case '-':
                    if (str[i + 1] == '-'){
                        tempList.add(new Token(Tag.DEC));
                        i++;
                    }
                    else{
                        tempList.add(new Token('-'));
                    }
                    break;
                case '*':
                    tempList.add(new Token('*'));
                    break;
                case '<':
                    if(str[i + 1] == '=') {
                        tempList.add(Word.le);
                        i++;
                    }
                    else {
                        tempList.add(new Token('<'));
                    }
                    break;
                case '>':
                    if (str[i + 1] == '=') {
                        tempList.add(Word.ge);
                        i++;
                    }
                    else {
                        tempList.add(new Token('>'));
                    }
                    break;
                default:
                    if (Character.isDigit(str[i])) {
                        StringBuilder sb = new StringBuilder(new String(str));
                        sb.delete(0, i);
                        Matcher mat = numPatterns.get(Tag.REAL).matcher(sb.toString());
                        if (mat.find()) {
                            if (mat.start() == 0) {
                                tempList.add(new Real(Double.parseDouble(mat.group())));
                                i = mat.end() + i - 1;
                                break;
                            }
                        }
                        mat = numPatterns.get(Tag.NUM).matcher(sb.toString());
                        if (mat.find()) {
                            if (mat.start() == 0) {
                                tempList.add(new Num(Integer.parseInt(mat.group())));
                                i = mat.end() + i - 1;
                                break;
                            }
                        }
                    }
                    else {
                        for (Integer x : patterns.keySet()) {
                            StringBuilder sb = new StringBuilder(new String(str));
                            sb.delete(0, i);
                            Matcher mat = patterns.get(x).matcher(sb.toString());
                            if (mat.find()) {
                                if (mat.start() == 0) {
                                    if (x == Tag.BASIC){
                                        switch(mat.group().charAt(0)){
                                            case 'i':
                                                tempList.add(Type.Int);
                                                break;
                                            case 'b':
                                                tempList.add(Type.Bool);
                                                break;
                                            case 'c':
                                                tempList.add(Type.Char);
                                                break;
                                            case 'f':
                                                tempList.add(Type.Float);
                                                break;
                                        }
                                        i = mat.end() + i - 2;
                                        break next;
                                    }
                                    if (x == Tag.STRING){
                                        tempList.add(new StringTag(mat.group()));
                                        i = mat.end() + i - 2;
                                        break next;
                                    }
                                    tempList.add(new Token(x));
                                    i = mat.end() + i - 2;
                                    break next;
                                }
                            }
                        }
                        StringBuilder sb = new StringBuilder(new String(str));
                        sb.delete(0, i);
                        Matcher mat = idpattern.matcher(sb.toString());
                        if (mat.find()) {
                            if (mat.start() == 0){
                                tempList.add(new Word(mat.group(),Tag.ID));
                                i = mat.end() + i - 1;
                                break;
                            }
                        }
                    }
                    break;
            }
        }
        return tempList;
    }
}