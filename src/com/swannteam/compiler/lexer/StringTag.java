package com.swannteam.compiler.lexer;

/**
 * Created by Geliogabalus on 30.05.2015.
 */
public class StringTag extends Token {
    public String value = "";

    public StringTag(String s) {
        super(Tag.STRING);
        value = s;
    }

    public String toString() {
        return value;
    }
}
