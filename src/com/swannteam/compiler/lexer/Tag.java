package com.swannteam.compiler.lexer;

/**
 * Created by Geliogabalus on 03.04.2015.
 */
public class Tag {
    public final static int
        AND = 256, OR = 257, XOR = 258,
        EQ = 259, GE = 260, LE = 261, NE = 262,
        ID = 263, BASIC = 264, NUM = 265, REAL = 266,
        DO = 267, WHIlE = 268, FOR = 269, BREAK = 270, CONTINUE = 271,
        INDEX = 272, MINUS = 273, TEMP = 274,
        IF = 275, THEN = 276, ELSE = 277, INC = 278, DEC = 279,
        TRUE = 280, FALSE = 281, PRINT = 282, STRING = 283;
}