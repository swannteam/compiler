package com.swannteam.compiler.lexer;

/**
 * Created by Geliogabalus on 03.04.2015.
 */
public class Token {
    private final int tag;

    public Token(int t) {
        tag = t;
    }

    public int getTag(){
        return tag;
    }

    public String toString(){
        return "" + (char)tag;
    }

    @Override
    public int hashCode() {
        return ((Integer)tag).hashCode();
    }
}