package com.swannteam.compiler.lexer;

/**
 * Created by Geliogabalus on 14.04.2015.
 */
public class Real extends Token {
    public final double value;

    public Real(double v) {
        super(Tag.REAL); value = v;
    }

    public String toString() {
        return "" + value;
    }
}