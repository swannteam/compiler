package com.swannteam.compiler.lexer;

/**
 * Created by Geliogabalus on 14.04.2015.
 */
public class ID extends Token {
    public final String value;

    public ID(String v) {
        super(Tag.ID);
        value = v;
    }

    public String toString() {
        return "" + value;
    }
}