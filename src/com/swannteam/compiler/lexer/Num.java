package com.swannteam.compiler.lexer;

/**
 * Created by Geliogabalus on 03.04.2015.
 */
public class Num extends Token{
    public final int value;

    public Num(int v) {
        super(Tag.NUM); value = v;
    }
    public
    String toString() {
        return "" + value;
    }
}