package com.swannteam.compiler.symbols;

import com.swannteam.compiler.inter.Id;
import com.swannteam.compiler.lexer.Token;

import java.util.Hashtable;

public class Env {
    private Hashtable<String, Id> table;
    protected Env prev;

    public Env(Env env) {
        table = new Hashtable<>();
        prev = env;
    }

    public void put(Token token, Id id) {
        table.put(token.toString(), id);
    }

    public Id get(Token w) {
        for (Env e = this; e != null; e = e.prev) {
            Id found = e.table.get(w.toString());
            if (found != null)
                return found;
        }
        return null;
    }
}