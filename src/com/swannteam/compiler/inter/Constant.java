package com.swannteam.compiler.inter;

import com.swannteam.compiler.lexer.Num;
import com.swannteam.compiler.lexer.Token;
import com.swannteam.compiler.symbols.Type;
import com.swannteam.compiler.lexer.Word;

public class Constant extends Expr {
    public Constant(Token tok, Type p) {
        super(tok, p);
    }

    public Constant(int i) {
        super(new Num(i), Type.Int);
    }

    public static final Constant
        True    = new Constant(Word.True,   Type.Bool),
        False   = new Constant(Word.False,  Type.Bool);

    public void jumping(int t, int f) {
        if (this == True && t != 0)
            emit(" goto L" + t);
        else if (this == False && f != 0)
            emit(" goto L" + f);
    }
}