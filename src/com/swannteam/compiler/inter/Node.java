package com.swannteam.compiler.inter;

import com.swannteam.compiler.lexer.Lexer;
import com.swannteam.compiler.parser.Parser;

public class Node {
    int lexline = 0;
    static int lables = 0;

    Node() {
        lexline = Lexer.line;
    }

    void error(String s) {
        throw new Error("line " + lexline + " " + s);
    }

    public int newlabel() {
        return ++lables;
    }

    public void emitLabel(int i) {
        System.out.println("\nL" + i + ":");
        Parser.codegen.sendlabel(i);
    }

    public void emit(String s) {
        System.out.print("\t" + s);
        Parser.codegen.send(s, this.getClass(), Parser.blockEnv);
    }
}