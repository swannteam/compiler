package com.swannteam.compiler.inter;

import com.swannteam.compiler.symbols.Type;
import com.swannteam.compiler.lexer.Word;
import com.swannteam.compiler.parser.Parser;
import com.swannteam.compiler.parser.Address;
import com.swannteam.compiler.parser.Var;

public class Temp extends Expr {
    static int count = 0;
    int number = 0;

    public Temp(Type p) {
        super(Word.temp, p);
        number = ++count;
        Parser.used += p.width;
        Parser.codegen.tempvars.add(new Var(new Address(Parser.used), Parser.blockEnv, "t" + number));
    }

    public String toString() {
        return "t" + number;
    }
}