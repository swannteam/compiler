package com.swannteam.compiler.inter;

import com.swannteam.compiler.symbols.Type;
import com.swannteam.compiler.lexer.Word;

public class Id extends Expr {
    public int offset;

    public Id(Word id, Type p, int b) {
        super(id, p);
        offset = b;
    }
}