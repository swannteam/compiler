package com.swannteam.compiler.inter;

/**
 * Created by Geliogabalus on 30.05.2015.
 */
public class Print extends Stmt {
    Expr expr;

    public Print(Expr x){
        expr = x;
    }

    public void gen(int b, int a){
        emit("<<" + expr.gen().toString());
    }

}
