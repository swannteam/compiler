package com.swannteam.compiler;

import com.swannteam.compiler.lexer.Lexer;
import com.swannteam.compiler.lexer.Token;
import com.swannteam.compiler.parser.Parser;
import com.swannteam.compiler.peconstructor.PEconstructor;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String filename = "code.txt";
        ArrayList<Token> tokens;
        try(BufferedReader fr = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8")))
        {
            tokens = lex.scan(fr);
        }  catch(Exception e) {
            System.out.println("File error!");
            return;
        }
        Parser parser = new Parser(tokens);
        parser.program();
        parser.codegen.fin();
        PEconstructor constr = new PEconstructor("out.exe", Parser.codegen.opcodes, parser.used, Parser.codegen.stringtable);
        try {
            constr.construct();
        } catch (IOException e) {
            e.printStackTrace();
        }
        constr.close();
    }
}